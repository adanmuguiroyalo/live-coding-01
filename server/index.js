const express = require('express')

const api = express()

api.use(express.urlencoded({ extended: false }))

api
  .use('/api', require('./routes/ApiRoutes'))
  .use((req, res) => res.sendStatus(404))

module.exports = {
  api,
}
