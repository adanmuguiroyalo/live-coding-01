const config = require('../../config.json')
const Placeholder = require('../../libs/Placeholder')

const getPosts = async (req, res) => {
  // Auth validation
  if (req.headers['x-api-key'] !== config.key) {
    return res.sendStatus(403)
  }

  const start = Number(req.query.start)
  const size = Number(req.query.size)
  const client = new Placeholder(config)
  const posts = await client.getPosts(start, size)
  if (!posts.length) {
    return res.sendStatus(404)
  }

  return res.send(posts)
}

module.exports = {
  getPosts,
}
