/* eslint-disable no-plusplus */
/* eslint-disable no-await-in-loop */
const asyncForEach = async (elements, cb) => {
  for (let index = 0; index < elements.length; index++) {
    await cb(elements[index], index, elements)
  }
}

module.exports = asyncForEach
