/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */
const fetch = require('node-fetch')
const asyncForEach = require('./async-foreach')

module.exports = class Placeholder {
  constructor(config) {
    this.baseUrl = config.placeholder.url
    this.users = []
  }

  async doRequest(url, method) {
    const headers = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    }
    const options = { method, headers }
    try {
      const response = await fetch(url, options)
      if (!response.ok) {
        throw new Error(`Status ${response.status}: ${response.statusText}`)
      }

      const result = await response.json()

      return result
    } catch (e) {
      console.error(`${this.doRequest.name} error`)
      console.error(e)

      return null
    }
  }

  async getComments(postId) {
    const comments = await this.doRequest(`${this.baseUrl}/posts/${postId}/comments`, 'GET')

    return comments || []
  }

  async getPosts(start, size) {
    const posts = await this.doRequest(`${this.baseUrl}/posts`, 'GET')
    if (!posts) {
      return []
    }

    try {
      const users = await this.getUsers()
      const end = start + size
      const pagePosts = posts.slice(start, end)
      const list = []
      await asyncForEach(pagePosts, async (post) => {
        const author = users.find((user) => user.id === post.userId)
        const comments = await this.getComments(post.id)

        post.user = author
        post.comments = comments
        list.push(post)
      })

      return list
    } catch (e) {
      console.error(`${this.getPosts.name} error`)
      console.error(e)

      return []
    }
  }

  async getUsers() {
    if (!this.users.length) {
      const users = await this.doRequest(`${this.baseUrl}/users`, 'GET')
      this.users = users || []
    }

    return this.users
  }
}
