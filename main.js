const { port, stage } = require('./config.json')
const { name, version } = require('./package.json')
const server = require('./server')

const main = async () => {
  console.log('================================================')
  console.log(`Starting up ${name}`)

  try {
    server.api.listen(port, () => {
      console.log(`${name} ${version} (${stage}) is running on port: ${port}`)
      console.log('================================================')
    })
  } catch (e) {
    console.error(`${main.name} error`, e)
  }
}

main()
